//
//  HotNewsView.h
//  Partner_123Xe
//
//  Created by TuTMT on 10/21/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol HotNewsViewDelegate <NSObject>
@end

@interface HotNewsView : UIView
@property (weak, nonatomic) IBOutlet UITableView *hotNewTableView;
@property (nonatomic, assign) id<HotNewsViewDelegate> delegate;

@end
