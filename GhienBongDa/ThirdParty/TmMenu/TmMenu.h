//
//  TmMenu.h
//  Passenger_123Xe
//
//  Created by TuTMT on 6/30/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WYPopoverController.h"
#import "MenuPopupTableViewCell.h"
#import "MenuPopupViewController.h"


@class MenuPopupViewController;
@protocol TmMenuDelegate <NSObject>
@required
-(void) ActionWithIndex:(int) index;

@end

@interface TmMenu : NSObject

#pragma mark property
@property (nonatomic,strong) NSArray *contenMenu;
@property (nonatomic,strong) NSString *urlCheckImage;
@property (nonatomic,strong) UIViewController *yourViewController;
@property (nonatomic,strong) UIView *yourUIView;
@property (nonatomic,strong) WYPopoverController* popoverMyController;



-(void) initView;
-(void) setDataWithYourUIView: (UIView *) yourUIView
                    urlCheckImage: (NSString *) urlCheckImage
                    listContenMenu: (NSArray *) contenMenu
                        width: (float) with;
-(IBAction)showPopover:(id)sender;

@property (nonatomic, assign) MenuPopupViewController *delegate;
@property (nonatomic, assign) id <TmMenuDelegate> delegateMenu;

@end
