//
//  TmMenu.m
//  Passenger_123Xe
//
//  Created by TuTMT on 6/30/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import "TmMenu.h"


@interface TmMenu ()<WYPopoverControllerDelegate,MenuPopupViewControllerDelegate>{
 //   WYPopoverController* popoverMyController;
    int numberCheck;
    float widthPopup;
}


@end

@implementation TmMenu

//- (void)viewDidLoad {
//    [super viewDidLoad];
//    
//    // Do any additional setup after loading the view.
//}
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}

-(void) initView{
    
    self.delegate = self;
    
  //  self.delegateMenu = self;
    
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MenuPopupViewController  *initialViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuPopupViewController"];
    [initialViewController setDataWithUIView:_yourUIView urlCheckImage:_urlCheckImage listContenMenu:_contenMenu];
    initialViewController.delegate = self;
    initialViewController.preferredContentSize = CGSizeMake(widthPopup, 50* _contenMenu.count);
    _yourViewController = initialViewController;

    
}


-(void) SendActionWithIndex:(int)index{
    NSLog(@"test");

    if ([self.delegateMenu respondsToSelector:@selector(ActionWithIndex:)]) {
        [self.delegateMenu ActionWithIndex:index];
    }
}

-(void) setDataWithYourUIView: (UIView *) yourUIView
                urlCheckImage: (NSString *) urlCheckImage
               listContenMenu: (NSArray *) contenMenu
                        width: (float) width
{
    self.yourUIView = yourUIView;
    self.urlCheckImage = urlCheckImage;
    self.contenMenu = contenMenu;
    widthPopup = width;
    [self initView];

}

#pragma mark WYPopoverControllerDelegate


-(void) dissmisPopup{
    [_popoverMyController dismissPopoverAnimated:true];

}


- (IBAction)showPopover:(id)sender
{
    _popoverMyController = [[WYPopoverController alloc] initWithContentViewController:_yourViewController];
    _popoverMyController.delegate = self;
   
    [_popoverMyController presentPopoverFromRect:_yourUIView.bounds inView:_yourUIView permittedArrowDirections:WYPopoverArrowDirectionAny animated:YES];
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    _popoverMyController.delegate = nil;
    _popoverMyController = nil;
}




@end
