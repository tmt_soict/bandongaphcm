//
//  MenuPopupViewController.h
//  Passenger_123Xe
//
//  Created by TuTMT on 6/29/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TmMenu.h"


@protocol MenuPopupViewControllerDelegate <NSObject>
@required
-(void) SendActionWithIndex: (int ) index;

@end

@interface MenuPopupViewController : UITableViewController

@property (nonatomic,strong) NSArray *menuArray;
@property (nonatomic,strong) NSString *urlCheckImage;
@property (nonatomic,strong) UIView *yourUIView;
@property (nonatomic,assign) id <MenuPopupViewControllerDelegate> delegate;

//@property (nonatomic,retain) JourneyListViewController *delegate;


#pragma mark function
-(void) setDataWithUIView: (UIView *) yourUIView
            urlCheckImage: (NSString *) urlCheckImage
           listContenMenu: (NSArray *) menuArray;


@end
