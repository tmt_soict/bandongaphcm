//
//  MenuPopupTableViewCell.h
//  Passenger_123Xe
//
//  Created by TuTMT on 6/30/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuPopupTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLable;

@property (weak, nonatomic) IBOutlet UIImageView *checkImageView;

@end
