//
//  MenuPopupTableViewCell.m
//  Passenger_123Xe
//
//  Created by TuTMT on 6/30/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import "MenuPopupTableViewCell.h"

@implementation MenuPopupTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
