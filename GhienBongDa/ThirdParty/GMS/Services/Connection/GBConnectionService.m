//
//  GBConnectionService.m
//  GoogleMapsTest
//
//  Created by Gabriel Alejandro Afonso Goncalves on 7/30/15.
//  Copyright (c) 2015 Gabriel Alejandro Afonso Goncalves. All rights reserved.
//

#import "GBConnectionService.h"
#import <AFNetworking/AFNetworking.h>
#import "Rows.h"
#import "Elements.h"
#import "Durational.h"
#import "Distance.h"
#import "BaseClassGSMDistance.h"
#import "Rows.h"
#import "Elements.h"
#import "Durational.h"
#import "Distance.h"
#import "BaseClassGSMDistance.h"
@interface GBConnectionService()

@end

@implementation GBConnectionService

- (void)GETRequestWithURL:(NSString *)url delegate:(id<GBConnectionServiceDelegate>)delegate
{
   


}

-(void) getDistanceWithURL:(NSString *) url delegate:(id<GBConnectionServiceDelegate>) delegate{
    NSLog(@"%@",url);
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
//        [delegate gotResponseDistanceObject:responseObject];
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        //ToDo: Do something if an error happend
//        NSLog(@"Error: %@", error);
//    }];
    
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:url parameters:nil error:nil];
    
    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        if (!error) {
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
             //   NSLog(@"JSON: %@", responseObject);
                NSDictionary *json = (NSDictionary*) responseObject;
                BaseClassGSMDistance* baseClass = [[BaseClassGSMDistance alloc] initWithDictionary:json];
                if(baseClass.rows.count > 0){
                    [delegate gotResponseDistanceObject:baseClass];

                }
                NSLog(@"%@", baseClass.status);
                
                
                
                
            }
        } else {
            [delegate gotResponseObject:nil];
            NSLog(@"Error: %@", error);

        }
    }] resume];

}
@end
