//
//  GBMapService.m
//  GoogleMapsTest
//
//  Created by Gabriel Alejandro Afonso Goncalves on 7/30/15.
//  Copyright (c) 2015 Gabriel Alejandro Afonso Goncalves. All rights reserved.
//

#import "GBMapService.h"
#import "GBConstants.h"
#import "GBConnectionService.h"
#import "GBTrip.h"
#import "GBPolyline.h"
#import "Rows.h"
#import "Elements.h"
#import "Durational.h"
#import "Distance.h"
#import "BaseClassGSMDistance.h"
@interface GBMapService() <GBConnectionServiceDelegate>

@property (nonatomic, strong) GBConnectionService *connectionService;
@property (nonatomic, strong) id<GBMapServiceDelegate> delegate;

@end

@implementation GBMapService

- (id)initWithDelegate:(id<GBMapServiceDelegate>)delegate
{
    self = [super init];
    if (self) {
        self.connectionService = [[GBConnectionService alloc] init];
        self.delegate = delegate;
    }
    
    return self;
}

- (void)routeFromOrigin:(CLLocationCoordinate2D)origin toDestination:(CLLocationCoordinate2D)destination
{
  
}



-(void) getDistance:(CLLocationCoordinate2D)origin toDestination:(CLLocationCoordinate2D)destination{
    
    NSString *originLatitude = [NSString stringWithFormat:@"%f", origin.latitude];
    NSString *originLongitude = [NSString stringWithFormat:@"%f", origin.longitude];
    NSString *destinationLatitude = [NSString stringWithFormat:@"%f", destination.latitude];
    NSString *destinationLongitude = [NSString stringWithFormat:@"%f", destination.longitude];
    
    NSString *urlRequestDistance = [NSString stringWithFormat:@"%@?origins=%@,%@&destinations=%@,%@&mode=driving&key=%@&language=vi", kGoogleMapsDistancematrixUrl, originLatitude, originLongitude, destinationLatitude, destinationLongitude, API_GOOGLE_KEY];
    
    [self.connectionService getDistanceWithURL:urlRequestDistance delegate:self];

}

#pragma mark GBConnectionServiceDelegate

- (void)gotResponseObject:(id)response
{
    
    @try {
        NSDictionary* json = response;
        NSMutableArray *routes = [[NSMutableArray alloc] init];
        int i = 0;
        NSDictionary* routeDic = (NSDictionary*) [[json objectForKey:kRoutes] firstObject];
        for (NSDictionary *route in [json objectForKey:kRoutes]) {
            if (i == 0) {
                
            }
            i ++;
            GBTrip *trip = [self.builder buildObjectForClass:NSStringFromClass([GBTrip class]) fromDictionary:route];
            if (trip) {
                [routes addObject:trip];
            }
        }
        
        GBTrip *trip = routes.firstObject;
        
        if (trip) {
            [self.delegate routeIdFetched:trip.polyline.overviewPolyline];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@", exception.description);
    }
}
-(void) gotResponseDistanceObject:(id)response{
    @try {
        if (response == nil) {
            [self.delegate resultGetDistance:0 distance:0 estimateTime:@""];
        }
        BaseClassGSMDistance *baseClass = (BaseClassGSMDistance*) response;
        
        if (baseClass.rows > 0) {
            Rows *rows = baseClass.rows[0];
            if (rows.elements.count >0) {
                Elements* elements = rows.elements[0];
                [self.delegate resultGetDistance:elements.duration.value distance:elements.distance.value estimateTime:elements.duration.text];
            }
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@", exception.description);
    }

}

@end
