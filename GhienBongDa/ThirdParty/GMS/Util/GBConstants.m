//
//  GBConstants.m
//  GoogleMapsTest
//
//  Created by Gabriel Alejandro Afonso Goncalves on 7/31/15.
//  Copyright (c) 2015 Gabriel Alejandro Afonso Goncalves. All rights reserved.
//

#import "GBConstants.h"

@implementation GBConstants

#pragma mark GoogleMaps

NSString *const kGoogleMapsDirectionsBaseUrl = @"http://maps.googleapis.com/maps/api/directions/json";
NSString *const kGoogleMapsDistancematrixUrl = @"https://maps.googleapis.com/maps/api/distancematrix/json";
NSString *const API_GOOGLE_KEY = @"AIzaSyC2ZedSAKebyAJxADjmHDiEhNsaglZUdi8";
//AIzaSyDP32_ckA1f9AzxyV4xXzg7A0Os0sblGRE
#pragma mark Polyline

NSString *const kPoints = @"points";
NSString *const kRoutes = @"routes";


#pragma -mark Distace 

NSString *const kOrigins        = @"origins";
NSString *const kDestinations   = @"destinations";
NSString *const kMode           = @"driving";
#pragma mark Trip

NSString *const kOverviewPolyline   = @"overview_polyline";
NSString *const kSummary            = @"summary";

@end
