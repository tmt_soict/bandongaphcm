//
//  Durational.h
//  Passenger_123Xe
//
//  Created by TuTMT on 11/17/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Durational : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double value;
@property (nonatomic, strong) NSString *text;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
