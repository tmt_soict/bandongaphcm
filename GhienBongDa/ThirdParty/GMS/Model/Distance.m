//
//  Distance.m
//
//  Created by TuTMT  on 11/17/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Distance.h"


NSString *const kDistanceValue = @"value";
NSString *const kDistanceText = @"text";


@interface Distance ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Distance

@synthesize value = _value;
@synthesize text = _text;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.value = [[self objectOrNilForKey:kDistanceValue fromDictionary:dict] doubleValue];
            self.text = [self objectOrNilForKey:kDistanceText fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.value] forKey:kDistanceValue];
    [mutableDict setValue:self.text forKey:kDistanceText];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.value = [aDecoder decodeDoubleForKey:kDistanceValue];
    self.text = [aDecoder decodeObjectForKey:kDistanceText];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_value forKey:kDistanceValue];
    [aCoder encodeObject:_text forKey:kDistanceText];
}

- (id)copyWithZone:(NSZone *)zone {
    Distance *copy = [[Distance alloc] init];
    
    
    
    if (copy) {

        copy.value = self.value;
        copy.text = [self.text copyWithZone:zone];
    }
    
    return copy;
}


@end
