//
//  BaseClassGSMDistance.m
//  Passenger_123Xe
//
//  Created by TuTMT on 11/17/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import "BaseClassGSMDistance.h"
#import "Rows.h"

NSString *const kBaseClassStatus = @"status";
NSString *const kBaseClassOriginAddresses = @"origin_addresses";
NSString *const kBaseClassDestinationAddresses = @"destination_addresses";
NSString *const kBaseClassRows = @"rows";

@interface BaseClassGSMDistance ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation BaseClassGSMDistance

@synthesize status = _status;
@synthesize originAddresses = _originAddresses;
@synthesize destinationAddresses = _destinationAddresses;
@synthesize rows = _rows;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.status = [self objectOrNilForKey:kBaseClassStatus fromDictionary:dict];
        self.originAddresses = [self objectOrNilForKey:kBaseClassOriginAddresses fromDictionary:dict];
        self.destinationAddresses = [self objectOrNilForKey:kBaseClassDestinationAddresses fromDictionary:dict];
        NSObject *receivedRows = [dict objectForKey:kBaseClassRows];
        NSMutableArray *parsedRows = [NSMutableArray array];
        
        if ([receivedRows isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedRows) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedRows addObject:[Rows modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedRows isKindOfClass:[NSDictionary class]]) {
            [parsedRows addObject:[Rows modelObjectWithDictionary:(NSDictionary *)receivedRows]];
        }
        
        self.rows = [NSArray arrayWithArray:parsedRows];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.status forKey:kBaseClassStatus];
    NSMutableArray *tempArrayForOriginAddresses = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.originAddresses) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForOriginAddresses addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForOriginAddresses addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForOriginAddresses] forKey:kBaseClassOriginAddresses];
    NSMutableArray *tempArrayForDestinationAddresses = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.destinationAddresses) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForDestinationAddresses addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForDestinationAddresses addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForDestinationAddresses] forKey:kBaseClassDestinationAddresses];
    NSMutableArray *tempArrayForRows = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.rows) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForRows addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForRows addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForRows] forKey:kBaseClassRows];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.status = [aDecoder decodeObjectForKey:kBaseClassStatus];
    self.originAddresses = [aDecoder decodeObjectForKey:kBaseClassOriginAddresses];
    self.destinationAddresses = [aDecoder decodeObjectForKey:kBaseClassDestinationAddresses];
    self.rows = [aDecoder decodeObjectForKey:kBaseClassRows];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_status forKey:kBaseClassStatus];
    [aCoder encodeObject:_originAddresses forKey:kBaseClassOriginAddresses];
    [aCoder encodeObject:_destinationAddresses forKey:kBaseClassDestinationAddresses];
    [aCoder encodeObject:_rows forKey:kBaseClassRows];
}

- (id)copyWithZone:(NSZone *)zone {
    BaseClassGSMDistance *copy = [[BaseClassGSMDistance alloc] init];
    
    
    
    if (copy) {
        
        copy.status = [self.status copyWithZone:zone];
        copy.originAddresses = [self.originAddresses copyWithZone:zone];
        copy.destinationAddresses = [self.destinationAddresses copyWithZone:zone];
        copy.rows = [self.rows copyWithZone:zone];
    }
    
    return copy;
}


@end
