//
//  BaseClassGSMDistance.h
//  Passenger_123Xe
//
//  Created by TuTMT on 11/17/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseClassGSMDistance : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSArray *originAddresses;
@property (nonatomic, strong) NSArray *destinationAddresses;
@property (nonatomic, strong) NSArray *rows;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
