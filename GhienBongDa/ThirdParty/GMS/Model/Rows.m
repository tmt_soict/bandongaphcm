//
//  Rows.m
//
//  Created by TuTMT  on 11/17/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Rows.h"
#import "Elements.h"

#import "Durational.h"
NSString *const kRowsElements = @"elements";


@interface Rows ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Rows

@synthesize elements = _elements;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedElements = [dict objectForKey:kRowsElements];
    NSMutableArray *parsedElements = [NSMutableArray array];
    
    if ([receivedElements isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedElements) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedElements addObject:[Elements modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedElements isKindOfClass:[NSDictionary class]]) {
       [parsedElements addObject:[Elements modelObjectWithDictionary:(NSDictionary *)receivedElements]];
    }

    self.elements = [NSArray arrayWithArray:parsedElements];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForElements = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.elements) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForElements addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForElements addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForElements] forKey:kRowsElements];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.elements = [aDecoder decodeObjectForKey:kRowsElements];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_elements forKey:kRowsElements];
}

- (id)copyWithZone:(NSZone *)zone {
    Rows *copy = [[Rows alloc] init];
    
    
    
    if (copy) {

        copy.elements = [self.elements copyWithZone:zone];
    }
    
    return copy;
}


@end
