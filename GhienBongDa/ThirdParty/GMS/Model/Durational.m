//
//  Durational.m
//  Passenger_123Xe
//
//  Created by TuTMT on 11/17/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import "Durational.h"
NSString *const kDurationalValue = @"value";
NSString *const kDurationalText = @"text";

@interface Durational ()
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;


@end
@implementation Durational

@synthesize value = _value;
@synthesize text = _text;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.value = [[self objectOrNilForKey:kDurationalValue fromDictionary:dict] doubleValue];
        self.text = [self objectOrNilForKey:kDurationalText fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.value] forKey:kDurationalValue];
    [mutableDict setValue:self.text forKey:kDurationalText];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.value = [aDecoder decodeDoubleForKey:kDurationalValue];
    self.text = [aDecoder decodeObjectForKey:kDurationalText];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeDouble:_value forKey:kDurationalValue];
    [aCoder encodeObject:_text forKey:kDurationalText];
}

- (id)copyWithZone:(NSZone *)zone {
    Durational *copy = [[Durational alloc] init];
    
    
    
    if (copy) {
        
        copy.value = self.value;
        copy.text = [self.text copyWithZone:zone];
    }
    
    return copy;
}


@end
