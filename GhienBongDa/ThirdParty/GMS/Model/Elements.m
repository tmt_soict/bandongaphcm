//
//  Elements.m
//
//  Created by TuTMT  on 11/17/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Elements.h"
#import "Durational.h"
#import "Distance.h"


NSString *const kElementsStatus = @"status";
NSString *const kElementsDuration = @"duration";
NSString *const kElementsDistance = @"distance";


@interface Elements ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Elements

@synthesize status = _status;
@synthesize duration = _duration;
@synthesize distance = _distance;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.status = [self objectOrNilForKey:kElementsStatus fromDictionary:dict];
            self.duration = [Durational modelObjectWithDictionary:[dict objectForKey:kElementsDuration]];
            self.distance = [Distance modelObjectWithDictionary:[dict objectForKey:kElementsDistance]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.status forKey:kElementsStatus];
    [mutableDict setValue:[self.duration dictionaryRepresentation] forKey:kElementsDuration];
    [mutableDict setValue:[self.distance dictionaryRepresentation] forKey:kElementsDistance];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.status = [aDecoder decodeObjectForKey:kElementsStatus];
    self.duration = [aDecoder decodeObjectForKey:kElementsDuration];
    self.distance = [aDecoder decodeObjectForKey:kElementsDistance];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_status forKey:kElementsStatus];
    [aCoder encodeObject:_duration forKey:kElementsDuration];
    [aCoder encodeObject:_distance forKey:kElementsDistance];
}

- (id)copyWithZone:(NSZone *)zone {
    Elements *copy = [[Elements alloc] init];
    
    
    
    if (copy) {

        copy.status = [self.status copyWithZone:zone];
        copy.duration = [self.duration copyWithZone:zone];
        copy.distance = [self.distance copyWithZone:zone];
    }
    
    return copy;
}


@end
