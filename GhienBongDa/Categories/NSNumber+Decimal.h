//
//  NSNumber+Decimal.h
//  paymentclient
//
//  Created by Nguyễn Hữu Hoà on 1/20/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Decimal)
+ (NSNumberFormatter*) currencyNumberFormatter;
- (NSString *)formatCurrency;
@end
