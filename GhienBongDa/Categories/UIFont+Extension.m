//
//  UIFont+SFUIDisplay.m
//  ZingMeMobile
//
//  Created by PhucPv on 10/14/15.
//  Copyright (c) 2015 VNG. All rights reserved.
//

#import "UIFont+Extension.h"

@implementation UIFont(Extension)
+ (UIFont *)robotoBoldWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"Roboto-Bold" size:size];
}
+ (UIFont *)robotoRegularWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"Roboto-Regular" size:size];
}
+ (UIFont *)robotoMediumWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"Roboto-Medium" size:size];
}
+ (UIFont *)robotoMediumItalicWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"Roboto-MediumItalic" size:size];
}
+ (UIFont *)helveticaNeueRegularWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"HelveticaNeue" size:size];
}
+ (UIFont *)helveticaNeueBoldWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"HelveticaNeue-Bold" size:size];
}
+ (UIFont *)helveticaNeueMediumWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"HelveticaNeue-Medium" size:size];
}
+ (void)dumpAllFonts{
    for (NSString *familyName in [UIFont familyNames]) {
        for (NSString *fontName in [UIFont fontNamesForFamilyName:familyName]) {
            NSLog(@"%@", fontName);
        }
    }
}

@end
