//
//  UIFont+SFUIDisplay.h
//  ZingMeMobile
//
//  Created by PhucPv on 10/14/15.
//  Copyright (c) 2015 VNG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont(Extension)
+ (UIFont *)robotoBoldWithSize:(CGFloat)size;
+ (UIFont *)robotoRegularWithSize:(CGFloat)size;
+ (UIFont *)robotoMediumWithSize:(CGFloat)size;
+ (UIFont *)robotoMediumItalicWithSize:(CGFloat)size;
+ (UIFont *)helveticaNeueRegularWithSize:(CGFloat)size;
+ (UIFont *)helveticaNeueBoldWithSize:(CGFloat)size;
+ (UIFont *)helveticaNeueMediumWithSize:(CGFloat)size;
+ (void)dumpAllFonts;
@end
