//
//  HTTPStatusCodes.h
//  PaymentUbusPartner
//
//  Created by Quang Hoang on 5/5/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#ifndef HTTPStatusCodes_h
#define HTTPStatusCodes_h

#define BAD_REQUEST 400

#define UNAUTHORIZED 401

#define PAYMENT_REQUIRED 402

#define FORBIDDEN 403

#define NOT_FOUND 404

//#define METHOD_NOT_ALLOWED 405

#define NOT_ACCEPTABLE 406

#define PROXY_AUTHENTICATION_REQUIRED 407

#define REQUEST_TIMEOUT 408

#define CONFLICT 409

#define GONE 410

#define LENGTH_REQUIRED 411

#define PRECONDITION_FAILED 412

#define REQUEST_ENTITY_TOO_LARGE 413

#define REQUEST_URI_TOO_LONG 414

#define UNSUPPORTED_MEDIA_TYPE 415

#define REQUESTED_RANGE_NOT_SATISFIABLE 416

#define EXPECTATION_FAILED 417

#define IM_A_TEAPOT 418

#define ENHANCE_YOUR_CALM 420

#define UNPROCESSABLE_ENTITY 422

#define LOCKED 423

#define FAILED_DEPENDENCY424

#define RESERVED_FOR_WEBDAV 425

#define UPGRADE_REQUIRED 426

#define PRECONDITION_REQUIRED 428

#define TOO_MANY_REQUESTS 429

#define REQUEST_HEADER_FIELDS_TOO_LARGE 431

#define NO_RESPONSE 444

#define RETRY_WITH 449

#define BLOCKED_BY_WINDOWS_PARENTAL_CONTROLS 450

#define UNAVAILABLE_FOR_LEGAL_REASONS 451

#define CLIENT_CLOSED_REQUEST 499

#define UNKNOWN_EXCEPTION 1001

#define METHOD_NOT_ALLOWED 1002

#define PARAM_ERROR 1003

#define SUCCSESS 0

#define PHONE_EXISTED 2001

#define PHONE_NOT_FOUND 2002

#define SESSION_EXPIRED 2003

#define WRONG_PASSWORD 2004

#define NOT_PERMISSION 2005

#define GET_OTP_ERROR 2010

#define LIMIT_GET_OTP 2011

#define OTP_NOT_FOUND 2012

#define OTP_EXPIRED 2013

#define TRIP_NOT_FOUND  2020

#define BOOKING_NOT_FOUND 2030

#define DRIVER_NOT_FOUND 2050 

#define DRIVER_MUST_JOIN_AN_OPERATOR 2051




#define API_VER @"2.0.0"
#define CTYPE 1


// Message error to user

#define mPHONE_EXISTED      @"Số điện thoại đã có trong hệ thống"
#define mSESSION_EXPIRE     @"Phiên đăng nhập đã hết hạn"
#define mWRONG_PASSWORD     @"Sai mật khẩu"
#define mNOT_FILLOUT_PHONE  @"Chưa nhập số điện thoại"
#define mWRONG_PHONE        @"Sai số điện thoại"
#define mNOT_FILLOUT_ALL    @"Không được bỏ trống"
#define mWRONG_RECENTPASS   @"Mật khẩu không khớp"
#define mPHONE_NOT_FOUND    @"Số điện thoại không có trong hệ thống"
#define mNOT_PERMISSION     @"Bạn không có quyền thực hiện"
#define mGET_OTP_ERROR      @"Lỗi get mã xác thực.Vui lòng thử lại"
#define mOTP_NOT_FOUND      @"Không tìm thấy mã xác thực"
#define mOTP_EXPIRED        @"Mã xác thực đã hết hạn"
#define mTRIP_NOT_FOUND     @"Không tìm thấy chuyến xe"
#endif /* HTTPStatusCodes_h */
