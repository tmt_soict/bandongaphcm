//
//  ListRainTracker.h
//
//  Created by Tran Tu on 5/28/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ListRainTracker : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double status;
@property (nonatomic, assign) double idtrangthai;
@property (nonatomic, assign) double idtram;
@property (nonatomic, strong) NSString *vitri;
@property (nonatomic, assign) double mucnuoc;
@property (nonatomic, strong) NSString *tentram;
@property (nonatomic, strong) NSString *trangthai;
@property (nonatomic, strong) NSString *viettat;
@property (nonatomic, strong) NSString *thoidiem;
@property (nonatomic, assign) double lat;
@property (nonatomic, assign) double lng;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
