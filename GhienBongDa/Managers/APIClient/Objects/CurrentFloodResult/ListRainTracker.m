//
//  ListRainTracker.m
//
//  Created by Tran Tu on 5/28/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "ListRainTracker.h"


NSString *const kListRainTrackerStatus = @"status";
NSString *const kListRainTrackerIdtrangthai = @"idtrangthai";
NSString *const kListRainTrackerIdtram = @"idtram";
NSString *const kListRainTrackerVitri = @"vitri";
NSString *const kListRainTrackerMucnuoc = @"mucnuoc";
NSString *const kListRainTrackerTentram = @"tentram";
NSString *const kListRainTrackerTrangthai = @"trangthai";
NSString *const kListRainTrackerViettat = @"viettat";
NSString *const kListRainTrackerThoidiem = @"thoidiem";
NSString *const kListRainTrackerLat = @"lat";
NSString *const kListRainTrackerLng = @"lng";


@interface ListRainTracker ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ListRainTracker

@synthesize status = _status;
@synthesize idtrangthai = _idtrangthai;
@synthesize idtram = _idtram;
@synthesize vitri = _vitri;
@synthesize mucnuoc = _mucnuoc;
@synthesize tentram = _tentram;
@synthesize trangthai = _trangthai;
@synthesize viettat = _viettat;
@synthesize thoidiem = _thoidiem;
@synthesize lat = _lat;
@synthesize lng = _lng;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.status = [[self objectOrNilForKey:kListRainTrackerStatus fromDictionary:dict] doubleValue];
            self.idtrangthai = [[self objectOrNilForKey:kListRainTrackerIdtrangthai fromDictionary:dict] doubleValue];
            self.idtram = [[self objectOrNilForKey:kListRainTrackerIdtram fromDictionary:dict] doubleValue];
            self.vitri = [self objectOrNilForKey:kListRainTrackerVitri fromDictionary:dict];
            self.mucnuoc = [[self objectOrNilForKey:kListRainTrackerMucnuoc fromDictionary:dict] doubleValue];
            self.tentram = [self objectOrNilForKey:kListRainTrackerTentram fromDictionary:dict];
            self.trangthai = [self objectOrNilForKey:kListRainTrackerTrangthai fromDictionary:dict];
            self.viettat = [self objectOrNilForKey:kListRainTrackerViettat fromDictionary:dict];
            self.thoidiem = [self objectOrNilForKey:kListRainTrackerThoidiem fromDictionary:dict];
            self.lat = [[self objectOrNilForKey:kListRainTrackerLat fromDictionary:dict] doubleValue];
            self.lng = [[self objectOrNilForKey:kListRainTrackerLng fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kListRainTrackerStatus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.idtrangthai] forKey:kListRainTrackerIdtrangthai];
    [mutableDict setValue:[NSNumber numberWithDouble:self.idtram] forKey:kListRainTrackerIdtram];
    [mutableDict setValue:self.vitri forKey:kListRainTrackerVitri];
    [mutableDict setValue:[NSNumber numberWithDouble:self.mucnuoc] forKey:kListRainTrackerMucnuoc];
    [mutableDict setValue:self.tentram forKey:kListRainTrackerTentram];
    [mutableDict setValue:self.trangthai forKey:kListRainTrackerTrangthai];
    [mutableDict setValue:self.viettat forKey:kListRainTrackerViettat];
    [mutableDict setValue:self.thoidiem forKey:kListRainTrackerThoidiem];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lat] forKey:kListRainTrackerLat];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lng] forKey:kListRainTrackerLng];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.status = [aDecoder decodeDoubleForKey:kListRainTrackerStatus];
    self.idtrangthai = [aDecoder decodeDoubleForKey:kListRainTrackerIdtrangthai];
    self.idtram = [aDecoder decodeDoubleForKey:kListRainTrackerIdtram];
    self.vitri = [aDecoder decodeObjectForKey:kListRainTrackerVitri];
    self.mucnuoc = [aDecoder decodeDoubleForKey:kListRainTrackerMucnuoc];
    self.tentram = [aDecoder decodeObjectForKey:kListRainTrackerTentram];
    self.trangthai = [aDecoder decodeObjectForKey:kListRainTrackerTrangthai];
    self.viettat = [aDecoder decodeObjectForKey:kListRainTrackerViettat];
    self.thoidiem = [aDecoder decodeObjectForKey:kListRainTrackerThoidiem];
    self.lat = [aDecoder decodeDoubleForKey:kListRainTrackerLat];
    self.lng = [aDecoder decodeDoubleForKey:kListRainTrackerLng];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_status forKey:kListRainTrackerStatus];
    [aCoder encodeDouble:_idtrangthai forKey:kListRainTrackerIdtrangthai];
    [aCoder encodeDouble:_idtram forKey:kListRainTrackerIdtram];
    [aCoder encodeObject:_vitri forKey:kListRainTrackerVitri];
    [aCoder encodeDouble:_mucnuoc forKey:kListRainTrackerMucnuoc];
    [aCoder encodeObject:_tentram forKey:kListRainTrackerTentram];
    [aCoder encodeObject:_trangthai forKey:kListRainTrackerTrangthai];
    [aCoder encodeObject:_viettat forKey:kListRainTrackerViettat];
    [aCoder encodeObject:_thoidiem forKey:kListRainTrackerThoidiem];
    [aCoder encodeDouble:_lat forKey:kListRainTrackerLat];
    [aCoder encodeDouble:_lng forKey:kListRainTrackerLng];
}

- (id)copyWithZone:(NSZone *)zone {
    ListRainTracker *copy = [[ListRainTracker alloc] init];
    
    
    
    if (copy) {

        copy.status = self.status;
        copy.idtrangthai = self.idtrangthai;
        copy.idtram = self.idtram;
        copy.vitri = [self.vitri copyWithZone:zone];
        copy.mucnuoc = self.mucnuoc;
        copy.tentram = [self.tentram copyWithZone:zone];
        copy.trangthai = [self.trangthai copyWithZone:zone];
        copy.viettat = [self.viettat copyWithZone:zone];
        copy.thoidiem = [self.thoidiem copyWithZone:zone];
        copy.lat = self.lat;
        copy.lng = self.lng;
    }
    
    return copy;
}


@end
