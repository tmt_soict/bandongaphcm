//
//  BaseClass.h
//
//  Created by Tran Tu on 5/28/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface BaseClass : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double err;
@property (nonatomic, strong) NSArray *listCameras;
@property (nonatomic, strong) NSArray *listTideTracker;
@property (nonatomic, strong) NSArray *listRainTracker;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
