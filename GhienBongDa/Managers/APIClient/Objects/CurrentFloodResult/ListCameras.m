//
//  ListCameras.m
//
//  Created by Tran Tu on 5/28/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "ListCameras.h"


NSString *const kListCamerasID = @"ID";
NSString *const kListCamerasAddress = @"Address";
NSString *const kListCamerasImageUrl = @"ImageUrl";
NSString *const kListCamerasLng = @"Lng";
NSString *const kListCamerasLat = @"Lat";


@interface ListCameras ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ListCameras

@synthesize iDProperty = _iDProperty;
@synthesize address = _address;
@synthesize imageUrl = _imageUrl;
@synthesize lng = _lng;
@synthesize lat = _lat;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iDProperty = [self objectOrNilForKey:kListCamerasID fromDictionary:dict];
            self.address = [self objectOrNilForKey:kListCamerasAddress fromDictionary:dict];
            self.imageUrl = [self objectOrNilForKey:kListCamerasImageUrl fromDictionary:dict];
            self.lng = [[self objectOrNilForKey:kListCamerasLng fromDictionary:dict] doubleValue];
            self.lat = [[self objectOrNilForKey:kListCamerasLat fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.iDProperty forKey:kListCamerasID];
    [mutableDict setValue:self.address forKey:kListCamerasAddress];
    [mutableDict setValue:self.imageUrl forKey:kListCamerasImageUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lng] forKey:kListCamerasLng];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lat] forKey:kListCamerasLat];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.iDProperty = [aDecoder decodeObjectForKey:kListCamerasID];
    self.address = [aDecoder decodeObjectForKey:kListCamerasAddress];
    self.imageUrl = [aDecoder decodeObjectForKey:kListCamerasImageUrl];
    self.lng = [aDecoder decodeDoubleForKey:kListCamerasLng];
    self.lat = [aDecoder decodeDoubleForKey:kListCamerasLat];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_iDProperty forKey:kListCamerasID];
    [aCoder encodeObject:_address forKey:kListCamerasAddress];
    [aCoder encodeObject:_imageUrl forKey:kListCamerasImageUrl];
    [aCoder encodeDouble:_lng forKey:kListCamerasLng];
    [aCoder encodeDouble:_lat forKey:kListCamerasLat];
}

- (id)copyWithZone:(NSZone *)zone {
    ListCameras *copy = [[ListCameras alloc] init];
    
    
    
    if (copy) {

        copy.iDProperty = [self.iDProperty copyWithZone:zone];
        copy.address = [self.address copyWithZone:zone];
        copy.imageUrl = [self.imageUrl copyWithZone:zone];
        copy.lng = self.lng;
        copy.lat = self.lat;
    }
    
    return copy;
}


@end
