//
//  ListTideTracker.h
//
//  Created by Tran Tu on 5/28/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ListTideTracker : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double listTideTrackerIdentifier;
@property (nonatomic, strong) NSString *vitri;
@property (nonatomic, strong) NSString *viettat;
@property (nonatomic, strong) NSString *tentrangthai;
@property (nonatomic, assign) double muctrieu;
@property (nonatomic, assign) double lat;
@property (nonatomic, strong) NSString *thoidiem;
@property (nonatomic, assign) double idtrangthai;
@property (nonatomic, strong) NSString *idtramtrieu;
@property (nonatomic, strong) NSString *tentramtrieu;
@property (nonatomic, assign) double lng;
@property (nonatomic, assign) double status;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
