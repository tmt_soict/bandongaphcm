//
//  ListTideTracker.m
//
//  Created by Tran Tu on 5/28/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "ListTideTracker.h"


NSString *const kListTideTrackerId = @"id";
NSString *const kListTideTrackerVitri = @"vitri";
NSString *const kListTideTrackerViettat = @"viettat";
NSString *const kListTideTrackerTentrangthai = @"tentrangthai";
NSString *const kListTideTrackerMuctrieu = @"muctrieu";
NSString *const kListTideTrackerLat = @"lat";
NSString *const kListTideTrackerThoidiem = @"thoidiem";
NSString *const kListTideTrackerIdtrangthai = @"idtrangthai";
NSString *const kListTideTrackerIdtramtrieu = @"idtramtrieu";
NSString *const kListTideTrackerTentramtrieu = @"tentramtrieu";
NSString *const kListTideTrackerLng = @"lng";
NSString *const kListTideTrackerStatus = @"status";


@interface ListTideTracker ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ListTideTracker

@synthesize listTideTrackerIdentifier = _listTideTrackerIdentifier;
@synthesize vitri = _vitri;
@synthesize viettat = _viettat;
@synthesize tentrangthai = _tentrangthai;
@synthesize muctrieu = _muctrieu;
@synthesize lat = _lat;
@synthesize thoidiem = _thoidiem;
@synthesize idtrangthai = _idtrangthai;
@synthesize idtramtrieu = _idtramtrieu;
@synthesize tentramtrieu = _tentramtrieu;
@synthesize lng = _lng;
@synthesize status = _status;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.listTideTrackerIdentifier = [[self objectOrNilForKey:kListTideTrackerId fromDictionary:dict] doubleValue];
            self.vitri = [self objectOrNilForKey:kListTideTrackerVitri fromDictionary:dict];
            self.viettat = [self objectOrNilForKey:kListTideTrackerViettat fromDictionary:dict];
            self.tentrangthai = [self objectOrNilForKey:kListTideTrackerTentrangthai fromDictionary:dict];
            self.muctrieu = [[self objectOrNilForKey:kListTideTrackerMuctrieu fromDictionary:dict] doubleValue];
            self.lat = [[self objectOrNilForKey:kListTideTrackerLat fromDictionary:dict] doubleValue];
            self.thoidiem = [self objectOrNilForKey:kListTideTrackerThoidiem fromDictionary:dict];
            self.idtrangthai = [[self objectOrNilForKey:kListTideTrackerIdtrangthai fromDictionary:dict] doubleValue];
            self.idtramtrieu = [self objectOrNilForKey:kListTideTrackerIdtramtrieu fromDictionary:dict];
            self.tentramtrieu = [self objectOrNilForKey:kListTideTrackerTentramtrieu fromDictionary:dict];
            self.lng = [[self objectOrNilForKey:kListTideTrackerLng fromDictionary:dict] doubleValue];
            self.status = [[self objectOrNilForKey:kListTideTrackerStatus fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.listTideTrackerIdentifier] forKey:kListTideTrackerId];
    [mutableDict setValue:self.vitri forKey:kListTideTrackerVitri];
    [mutableDict setValue:self.viettat forKey:kListTideTrackerViettat];
    [mutableDict setValue:self.tentrangthai forKey:kListTideTrackerTentrangthai];
    [mutableDict setValue:[NSNumber numberWithDouble:self.muctrieu] forKey:kListTideTrackerMuctrieu];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lat] forKey:kListTideTrackerLat];
    [mutableDict setValue:self.thoidiem forKey:kListTideTrackerThoidiem];
    [mutableDict setValue:[NSNumber numberWithDouble:self.idtrangthai] forKey:kListTideTrackerIdtrangthai];
    [mutableDict setValue:self.idtramtrieu forKey:kListTideTrackerIdtramtrieu];
    [mutableDict setValue:self.tentramtrieu forKey:kListTideTrackerTentramtrieu];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lng] forKey:kListTideTrackerLng];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kListTideTrackerStatus];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.listTideTrackerIdentifier = [aDecoder decodeDoubleForKey:kListTideTrackerId];
    self.vitri = [aDecoder decodeObjectForKey:kListTideTrackerVitri];
    self.viettat = [aDecoder decodeObjectForKey:kListTideTrackerViettat];
    self.tentrangthai = [aDecoder decodeObjectForKey:kListTideTrackerTentrangthai];
    self.muctrieu = [aDecoder decodeDoubleForKey:kListTideTrackerMuctrieu];
    self.lat = [aDecoder decodeDoubleForKey:kListTideTrackerLat];
    self.thoidiem = [aDecoder decodeObjectForKey:kListTideTrackerThoidiem];
    self.idtrangthai = [aDecoder decodeDoubleForKey:kListTideTrackerIdtrangthai];
    self.idtramtrieu = [aDecoder decodeObjectForKey:kListTideTrackerIdtramtrieu];
    self.tentramtrieu = [aDecoder decodeObjectForKey:kListTideTrackerTentramtrieu];
    self.lng = [aDecoder decodeDoubleForKey:kListTideTrackerLng];
    self.status = [aDecoder decodeDoubleForKey:kListTideTrackerStatus];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_listTideTrackerIdentifier forKey:kListTideTrackerId];
    [aCoder encodeObject:_vitri forKey:kListTideTrackerVitri];
    [aCoder encodeObject:_viettat forKey:kListTideTrackerViettat];
    [aCoder encodeObject:_tentrangthai forKey:kListTideTrackerTentrangthai];
    [aCoder encodeDouble:_muctrieu forKey:kListTideTrackerMuctrieu];
    [aCoder encodeDouble:_lat forKey:kListTideTrackerLat];
    [aCoder encodeObject:_thoidiem forKey:kListTideTrackerThoidiem];
    [aCoder encodeDouble:_idtrangthai forKey:kListTideTrackerIdtrangthai];
    [aCoder encodeObject:_idtramtrieu forKey:kListTideTrackerIdtramtrieu];
    [aCoder encodeObject:_tentramtrieu forKey:kListTideTrackerTentramtrieu];
    [aCoder encodeDouble:_lng forKey:kListTideTrackerLng];
    [aCoder encodeDouble:_status forKey:kListTideTrackerStatus];
}

- (id)copyWithZone:(NSZone *)zone {
    ListTideTracker *copy = [[ListTideTracker alloc] init];
    
    
    
    if (copy) {

        copy.listTideTrackerIdentifier = self.listTideTrackerIdentifier;
        copy.vitri = [self.vitri copyWithZone:zone];
        copy.viettat = [self.viettat copyWithZone:zone];
        copy.tentrangthai = [self.tentrangthai copyWithZone:zone];
        copy.muctrieu = self.muctrieu;
        copy.lat = self.lat;
        copy.thoidiem = [self.thoidiem copyWithZone:zone];
        copy.idtrangthai = self.idtrangthai;
        copy.idtramtrieu = [self.idtramtrieu copyWithZone:zone];
        copy.tentramtrieu = [self.tentramtrieu copyWithZone:zone];
        copy.lng = self.lng;
        copy.status = self.status;
    }
    
    return copy;
}


@end
