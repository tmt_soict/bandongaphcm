//
//  BaseClass.m
//
//  Created by Tran Tu on 5/28/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "BaseClass.h"
#import "ListCameras.h"
#import "ListTideTracker.h"
#import "ListRainTracker.h"


NSString *const kBaseClassErr = @"err";
NSString *const kBaseClassListCameras = @"list_cameras";
NSString *const kBaseClassListTideTracker = @"list_tide_tracker";
NSString *const kBaseClassListRainTracker = @"list_rain_tracker";


@interface BaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation BaseClass

@synthesize err = _err;
@synthesize listCameras = _listCameras;
@synthesize listTideTracker = _listTideTracker;
@synthesize listRainTracker = _listRainTracker;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.err = [[self objectOrNilForKey:kBaseClassErr fromDictionary:dict] doubleValue];
    NSObject *receivedListCameras = [dict objectForKey:kBaseClassListCameras];
    NSMutableArray *parsedListCameras = [NSMutableArray array];
    
    if ([receivedListCameras isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedListCameras) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedListCameras addObject:[ListCameras modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedListCameras isKindOfClass:[NSDictionary class]]) {
       [parsedListCameras addObject:[ListCameras modelObjectWithDictionary:(NSDictionary *)receivedListCameras]];
    }

    self.listCameras = [NSArray arrayWithArray:parsedListCameras];
    NSObject *receivedListTideTracker = [dict objectForKey:kBaseClassListTideTracker];
    NSMutableArray *parsedListTideTracker = [NSMutableArray array];
    
    if ([receivedListTideTracker isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedListTideTracker) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedListTideTracker addObject:[ListTideTracker modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedListTideTracker isKindOfClass:[NSDictionary class]]) {
       [parsedListTideTracker addObject:[ListTideTracker modelObjectWithDictionary:(NSDictionary *)receivedListTideTracker]];
    }

    self.listTideTracker = [NSArray arrayWithArray:parsedListTideTracker];
    NSObject *receivedListRainTracker = [dict objectForKey:kBaseClassListRainTracker];
    NSMutableArray *parsedListRainTracker = [NSMutableArray array];
    
    if ([receivedListRainTracker isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedListRainTracker) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedListRainTracker addObject:[ListRainTracker modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedListRainTracker isKindOfClass:[NSDictionary class]]) {
       [parsedListRainTracker addObject:[ListRainTracker modelObjectWithDictionary:(NSDictionary *)receivedListRainTracker]];
    }

    self.listRainTracker = [NSArray arrayWithArray:parsedListRainTracker];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.err] forKey:kBaseClassErr];
    NSMutableArray *tempArrayForListCameras = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.listCameras) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForListCameras addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForListCameras addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForListCameras] forKey:kBaseClassListCameras];
    NSMutableArray *tempArrayForListTideTracker = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.listTideTracker) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForListTideTracker addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForListTideTracker addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForListTideTracker] forKey:kBaseClassListTideTracker];
    NSMutableArray *tempArrayForListRainTracker = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.listRainTracker) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForListRainTracker addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForListRainTracker addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForListRainTracker] forKey:kBaseClassListRainTracker];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.err = [aDecoder decodeDoubleForKey:kBaseClassErr];
    self.listCameras = [aDecoder decodeObjectForKey:kBaseClassListCameras];
    self.listTideTracker = [aDecoder decodeObjectForKey:kBaseClassListTideTracker];
    self.listRainTracker = [aDecoder decodeObjectForKey:kBaseClassListRainTracker];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_err forKey:kBaseClassErr];
    [aCoder encodeObject:_listCameras forKey:kBaseClassListCameras];
    [aCoder encodeObject:_listTideTracker forKey:kBaseClassListTideTracker];
    [aCoder encodeObject:_listRainTracker forKey:kBaseClassListRainTracker];
}

- (id)copyWithZone:(NSZone *)zone {
    BaseClass *copy = [[BaseClass alloc] init];
    
    
    
    if (copy) {

        copy.err = self.err;
        copy.listCameras = [self.listCameras copyWithZone:zone];
        copy.listTideTracker = [self.listTideTracker copyWithZone:zone];
        copy.listRainTracker = [self.listRainTracker copyWithZone:zone];
    }
    
    return copy;
}


@end
