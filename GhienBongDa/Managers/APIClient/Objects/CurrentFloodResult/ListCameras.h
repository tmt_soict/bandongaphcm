//
//  ListCameras.h
//
//  Created by Tran Tu on 5/28/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ListCameras : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *iDProperty;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, assign) double lng;
@property (nonatomic, assign) double lat;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
