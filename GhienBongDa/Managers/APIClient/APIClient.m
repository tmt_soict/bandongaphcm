//
//  APIClient.m
//  Shoppie
//
//  Created by Le Quang Vinh on 11/13/13.
//
//

#import "APIClient.h"
#import "Global.h"
#import "URLConstant.h"
#import "AppData.h"
#import "AppDelegate.h"

#define API_VERSION @"1.01"
#define API_VERSION_KEY @"api_ver"
#define SESSION_KEY @"sessionkey"

// Objects



@implementation APIClient

#pragma mark- Public methods

+ (APIClient *)sharedClient {
    static APIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[APIClient alloc] initWithBaseURL:[NSURL URLWithString:kBaseURL]];
    });
    
    return _sharedClient;
}

- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
    
    [self registerHTTPOperationClass:[SAHTTPRequestOperation class]];
    
    return self;
}

#pragma -mark get article
-(void)getAddressWithLat:(NSString*)lat
                     lng:(NSString*) lng
       completionHandler: (void (^)(NSString* output, NSError* error, double errorCode)) handler{
//    NSString* urlRequest = [NSString stringWithFormat:@"%@%@=%@,%@&%@=%@", BASE_URL_GOOGLE,LATLNG,lat,lng,KEY,KEY_GG_API];
//    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//    
//    
//    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:urlRequest parameters:nil error:nil];
//    
//    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
//    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
//        
//        if (!error) {
//            if ([responseObject isKindOfClass:[NSDictionary class]]) {
//                //blah blah
//                if (handler) {
//                    NSDictionary* baseClass = [responseObject[@"results"] firstObject];
//                    Results* result = [[Results alloc] initWithDictionary:baseClass];
//                    NSString* formatAddress = result.formattedAddress;
//                    NSLog(@"%@",formatAddress);
//                    NSLog(@"-------------------finish");
//                    NSString* status = (NSString*) responseObject[@"status"];
//                    if ([status  isEqual: @"OK"]) {
//                        handler(formatAddress,error, 200);
//                        
//                    }
//                    else{
//                        handler(nil,error, 0);
//                        
//                    }
//                }
//            }
//        } else {
//            if (handler) {
//                handler(nil,error,0);
//            }
//        }
//    }] resume];
    
}
-(void) getCurrentFloodInCityWithDate:(NSDate*) date
                    completionHandler: (void (^)(BaseClass* output, NSError* error, double errorCode)) handler{
    NSString *escapedString = [BASE_URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    NSString* urlRequest = [NSString stringWithFormat:@"%@",escapedString];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
        NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:urlRequest parameters:nil error:nil];
    
        req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    securityPolicy.allowInvalidCertificates = YES;
    
    [securityPolicy setValidatesDomainName:NO];
    manager.securityPolicy = securityPolicy;
        [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
    
            if (!error) {
                if ([responseObject isKindOfClass:[NSDictionary class]]) {
                    //blah blah
                    if (handler) {
                        BaseClass* currentBaseClass = [[BaseClass alloc]initWithDictionary:responseObject];
                        handler(currentBaseClass,error,currentBaseClass.err);
                    }
                }
            } else {
                if (handler) {
                    handler(nil,error,0);
                }
            }
        }] resume];
    
}

@end
