//
//  APIClient.h
//  Shoppie
//
//  Created by Le Quang Vinh on 11/13/13.
//
//

#import <UIKit/UIKit.h>
#import "SAHTTPClient.h"
#import "SAImageRequestOperation.h"
#import "DataModels.h"


/***************************BaseURI******************/

#define BASE_URL_GOOGLE     @"https://maps.googleapis.com/maps/api/geocode/json?"
#define BASE_URL            @"https://udimap.mẹsữa.vn/current_info"


#define KEY_GG_API          @"AIzaSyAui8ukfZS8FxsGln6CtrcyG_zDSyePSuQ"

@interface APIClient : SAHTTPClient

#pragma mark- Public methods

+ (APIClient *)sharedClient;

//Define all API functions here

#pragma mark- get Article 
-(void)getArticleWithSid:(NSString*)sid
                     cid:(NSString*) cid
                   count:(NSNumber*)count
                    meta:(BOOL) meta
                 lastLid:(NSString*) lid
       completionHandler: (void (^)(BaseClass* output, NSError* error, double  errorCode)) handler;
#pragma mark- test 
//-(void) uploadTestWithData:(NSString*) name;



#pragma getAddress with lat lng 
-(void)getAddressWithLat:(NSString*)lat
                     lng:(NSString*) lng
       completionHandler: (void (^)(NSString* output, NSError* error, double errorCode)) handler;

#pragma current flood 
-(void) getCurrentFloodInCityWithDate:(NSDate*) date
                    completionHandler: (void (^)(BaseClass* output, NSError* error, double errorCode)) handler;

@end
