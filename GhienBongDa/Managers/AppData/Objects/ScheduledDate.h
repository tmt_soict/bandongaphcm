//
//  ScheduledDate.h
//  PaymentUbusPartner
//
//  Created by QuangKomodo on 22/03/2016.
//  Copyright © Năm 2016 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Storable.h"

typedef enum : NSUInteger {
    ScheduleDateTypeBusy,
    SchedulegDateTypeWait,
    SchedulegDateTypeFree,
} ScheduledDateType;

@interface ScheduledDate : NSObject<Storable>
@property (nonatomic, retain) NSDate *date;
@property (nonatomic, retain) NSNumber *lat;
@property (nonatomic, retain) NSNumber *lng;
@property ScheduledDateType type;
@end
