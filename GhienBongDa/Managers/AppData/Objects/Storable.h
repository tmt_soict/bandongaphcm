//
//  Storable.h
//  PhotoEditor
//
//  Created by tinvk on 12/30/14.
//  Copyright (c) 2014 CALACULU. All rights reserved.
//

#ifndef PhotoEditor_Storable_h
#define PhotoEditor_Storable_h

#import <Foundation/Foundation.h>

@protocol Storable<NSObject>

- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)decoder;

@end

#endif
