//
//  ScheduledDate.m
//  PaymentUbusPartner
//
//  Created by QuangKomodo on 22/03/2016.
//  Copyright © Năm 2016 VNG Corporation. All rights reserved.
//

#import "ScheduledDate.h"

@implementation ScheduledDate
@synthesize date,lat,lng;
- (void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:date forKey:@"scheduledDate"];
    [encoder encodeObject:lat forKey:@"lat"];
    [encoder encodeObject:lng forKey:@"lng"];
    [encoder encodeObject:[NSNumber numberWithInt:self.type] forKey:@"typeScheduledDate"];
}

- (id)initWithCoder:(NSCoder *)decoder{
    if ((self = [super init])) {
        self.date = [decoder decodeObjectForKey:@"scheduledDate"];
        self.lat = [decoder decodeObjectForKey:@"lat"];
        self.lng = [decoder decodeObjectForKey:@"lng"];
        self.type = [[decoder decodeObjectForKey:@"typeScheduledDate"] intValue];
    }
    return self;
}
@end
