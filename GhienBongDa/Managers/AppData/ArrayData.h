//
//  ArrayData.h
//  PaymentUbusPartner
//
//  Created by QuangKomodo on 22/03/2016.
//  Copyright © Năm 2016 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ArrayData : NSObject

#pragma mark- Storage utility
+ (ArrayData *)sharedInstance;

- (void)writeArrayWithCustomObjToUserDefaults:(NSString *)keyName withArray:(NSMutableArray *)myArray;
- (NSMutableArray *)readArrayWithCustomObjFromUserDefaults:(NSString*)keyName;

#pragma mark- Public methods

- (void)resetData;

#pragma mark- Array data API
-(void)setScheduledDates:(NSMutableArray *)scheduledDates;
-(NSMutableArray *)scheduledDates;

@end
