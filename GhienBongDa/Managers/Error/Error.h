//
//  Error.h
//  Partner_123Xe
//
//  Created by TuTMT on 9/15/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#ifndef Error_h
#define Error_h
#import <Foundation/Foundation.h>

//SESSION EXPIRE

//#define SESSION_EXPIRE  @2003
//#define WRONG_PASSWORD  @2004
//#define LIMIT_GET_OTP   @2011
//#define PHONE_EXISTED  @2001

// Message to user

#define mPHONE_EXISTED      @"Số điện thoại đã có trong hệ thống"
#define mSESSION_EXPIRE     @"Phiên đăng nhập đã hết hạn"
#define mWRONG_PASSWORD     @"Sai mật khẩu"
#define mNOT_FILLOUT_PHONE  @"Chưa nhập số điện thoại"
#define mWRONG_PHONE        @"Sai số điện thoại"
#define mNOT_FILLOUT_ALL    @"Không được bỏ trống"


#endif /* Error_h */
