//
//  GlobalObjects.h
//  PhotoEditor
//
//  Created by tinvk on 1/5/15.
//  Copyright (c) 2015 CALACULU. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CWLSynthesizeSingleton.h"

//#import "LeftSlideMenuTableViewController.h"

@interface GlobalObjects : NSObject

//CWL_DECLARE_SINGLETON_FOR_CLASS(GlobalObjects)

+ (instancetype)sharedInstance;
@end
