//
//  URLConstant.h
//  Plus
//
//  Created by Wala on 2/16/14.
//
//

#import <Foundation/Foundation.h>

@interface URLConstant : NSObject

extern NSString* const BASE_URL;
extern NSString* const SESSION;
extern NSString* const USER;
extern NSString* const DRIVER;

@end
