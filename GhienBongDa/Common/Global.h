//
//  Global.h
//  Shoppie
//
//  Created by Le Quang Vinh on 11/13/13.
//  
//

#ifndef Shoppie_Global_h
#define Shoppie_Global_h

#import "Logging.h"

//Some useful macro
#import "Macro.h"

//Define all key here
#define kBaseURL @""
#define IPAD_XIB_POSTFIX @"~iPad"

//Common class
#import "Util.h"

// Constants
#define kCacheDriverInfo    @"driver_info"

#endif
