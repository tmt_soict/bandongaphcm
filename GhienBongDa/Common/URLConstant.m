//
//  URLConstant.m
//  Plus
//
//  Created by Wala on 2/16/14.
//
//

#import "URLConstant.h"

@implementation URLConstant

NSString * const SESSION                         =   @"/uodapi/session";
NSString * const USER                            =   @"/uodapi/user";
NSString * const DRIVER                          =   @"/uodapi/driver";

@end
