//
//  MenuItem.h
//  paymentclient
//
//  Created by tinvukhac on 12/8/15.
//  Copyright © 2015 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
typedef enum {
    menuIndexHome,
    menuIndexPreditect,
    menuIndexDirection,
    menuIndexNewFlood,
    menuIndexSetting,
    menuIndexSuport,
} menuIndexType;

typedef void (^BlockAction)(void);
@interface MenuItem : NSObject

#pragma mark- Properties
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * imageName;
@property (nonatomic, strong) UIImage *normalImage;
@property (nonatomic, strong) UIImage *hoverImage;
@property (nonatomic, copy) BlockAction immediateAcion;
@property (nonatomic, copy) BlockAction action;
@property (nonatomic) menuIndexType menuIndex;


#pragma mark- Public methods

- (id)initWithTitle:(NSString *)title imageName:(NSString *)imageName;
- (id)initWithTitle:(NSString *)title
          imageName:(NSString *)imageName
          completion:(BlockAction) completion;

@end
@interface ItemData : NSObject
@property (nonatomic, copy)  BlockAction completion;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *imageName;
@end
