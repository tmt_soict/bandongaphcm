//
//  Util.h
//  Shoppie
//
//  Created by Le Quang Vinh on 11/13/13.
//  
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "MBProgressHUD.h"
#import "AppDelegate.h"

@interface Util : NSObject

@property (strong, nonatomic) MBProgressHUD *progressView;

+ (Util *)sharedUtil;
+ (AppDelegate *)appDelegate;

//coder radius
#pragma mark set coder radius
+ (UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius;
#pragma -mark time 
+ (NSString *)timeFormatted:(int)totalSeconds;

//Alert functions
+ (void)showMessage:(NSString *)message withTitle:(NSString *)title;
+ (void)showMessage:(NSString *)message withTitle:(NSString *)title andDelegate:(id)delegate;
+ (void)showMessage:(NSString *)message withTitle:(NSString *)title delegate:(id)delegate andTag:(NSInteger)tag;

+ (void)showMessage:(NSString *)message withTitle:(NSString *)title delegate:(id)delegate titleButton:(NSString *) titleButton andTag:(NSInteger)tag;

+ (void)showMessage:(NSString *)message withTitle:(NSString *)title cancelButtonTitle:(NSString *)cancelTitle otherButtonTitles:(NSString *)otherTitle delegate:(id)delegate andTag:(NSInteger)tag;
+ (void)showMessage:(NSString *)message withTitle:(NSString *)title style:(UIAlertViewStyle)style cancelButtonTitle:(NSString *)cancelTitle otherButtonTitles:(NSString *)otherTitle delegate:(id)delegate andTag:(NSInteger)tag;

//Date functions
+ (NSDate *)dateFromString:(NSString *)dateString withFormat:(NSString *)dateFormat;
+ (NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)dateFormat;
+ (NSString *)stringFromDateString:(NSString *)dateString;
+ (NSDate*)convertTwitterDateToNSDate:(NSString*)created_at;
+ (NSString *)stringFromDateRelative:(NSDate*)date;

//Loading view functions
- (void)showLoadingView;
- (void)showLoadingViewWithTitle:(NSString *)title;
- (void)hideLoadingView;

//NSUserDefaults functions
+ (void)setValue:(id)value forKey:(NSString *)key;
+ (void)setValue:(id)value forKeyPath:(NSString *)keyPath;
+ (void)setObject:(id)obj forKey:(NSString *)key;
+ (id)valueForKey:(NSString *)key;
+ (id)valueForKeyPath:(NSString *)keyPath;
+ (id)objectForKey:(NSString *)key;
+ (void)removeObjectForKey:(NSString *)key;

//JSON functions
+ (id)convertJSONToObject:(NSString*)str;
+ (NSString *)convertObjectToJSON:(id)obj;
+ (id)getJSONObjectFromFile:(NSString *)file;

//Other stuff
+ (NSString *)getXIB:(Class)fromClass;

#pragma mark- My helper (tinvk)

+ (int)getIntValueWithDictionary:(NSDictionary*)dict forKey:(NSString*)key;
+ (float)getFloatValueWithDictionary:(NSDictionary*)dict forKey:(NSString*)key;

+ (double)computeDistanceWithLat1:(double)lat1 long1:(double)long1 lat2:(double)lat2 long2:(double)long2;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

+ (NSString*)convertToCurrencyWithNumber:(int)intNumber;
+ (NSString*)convertToCurrencyWithFloat:(float)floatNumber;
+ (NSString*)convertToCurrencyWithString:(NSString *)stringNumber;

+ (NSString*)formatToSeparatedStyleWithString:(NSString *)str;
+ (NSString*)stringWithDashSeparated:(NSString*)str;

+ (BOOL)isValidNickname:(NSString*)input;

+ (CGSize) computeSizeWithString:(NSString *)input font:(UIFont *)font;

+ (UIImage *) imageForAsset:(ALAsset*) aAsset;

+ (UIImage *) fullScreenImageForAsset:(ALAsset*) aAsset;

+ (CGImageRef)resizeCGImage:(CGImageRef)image toWidth:(unsigned long)width andHeight:(unsigned long)height;

+ (NSString *)getStoryBoardName;

+ (UINavigationController *)getNavigationControllerWithIdentifier:(NSString *)identifier storyboardName:(NSString *)storyboardName;

// App version
+ (NSString *) appVersion;
+ (NSString *) build;
+ (NSString *) versionBuild;

// UIImage
/*Drawimage in image*/
+ (UIImage*) drawImage:(UIImage*)fgImage inImage:(UIImage*)bgImage atPoint:(CGPoint)point;

/*Create image width color*/
+ (UIImage *)imageWithColor:(UIColor *)color;

/*Create image width color and size*/
+ (UIImage *)imageWithColor:(UIColor *)color andSize:(CGSize)size;

/*Create QR image from a string*/
+ (CIImage *)createQRForString:(NSString *)qrString;

@end
