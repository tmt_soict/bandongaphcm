//
//  NewFloodingPointViewController.m
//  GhienBongDa
//
//  Created by Tran Minh Tu on 5/26/17.
//  Copyright © 2017 TuTMT. All rights reserved.
//

#import "NewFloodingPointViewController.h"

@interface NewFloodingPointViewController ()

@end

@implementation NewFloodingPointViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUiBarForSlideMenuWithImageName:@"navigation_ic"];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
