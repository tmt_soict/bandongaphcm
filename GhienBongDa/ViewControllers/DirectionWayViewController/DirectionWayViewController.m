//
//  DirectionWayViewController.m
//  GhienBongDa
//
//  Created by Tran Minh Tu on 5/26/17.
//  Copyright © 2017 TuTMT. All rights reserved.
//

#import "DirectionWayViewController.h"

@interface DirectionWayViewController ()

@end

@implementation DirectionWayViewController{
    CLLocationManager* locationManager;
    int indexSelectLocation;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupDirectionWayViewController];
    // Do any additional setup after loading the view.
}

-(void) setupDirectionWayViewController{
    self.searchView.layer.borderWidth = 1;
    self.searchView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.searchView.layer.cornerRadius = 10;
    [self setupUiBarForSlideMenuWithImageName:@"navigation_ic"];

    [self prepareGoogleMaps];
}
- (void)prepareGoogleMaps
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    GMSCameraPosition *camera;
    if (locationManager != nil) {
        GMSCameraUpdate *vancouverCam = [GMSCameraUpdate setTarget:locationManager.location.coordinate zoom:12.0f];
        
        [self.gMapView moveCamera:vancouverCam];
        [self.gMapView animateWithCameraUpdate:vancouverCam];
    }
    else{
        camera = [GMSCameraPosition cameraWithLatitude:-33.86 longitude:151.20 zoom:12];
        
    }
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, 120, 0);
    [self.gMapView setPadding:edgeInsets];
    self.gMapView.myLocationEnabled = YES;
    self.gMapView.delegate = self;
    self.gMapView.indoorEnabled = YES;
    self.gMapView.accessibilityElementsHidden = NO;
    self.gMapView.settings.scrollGestures = YES;
    self.gMapView.settings.zoomGestures = YES;
    self.gMapView.settings.compassButton = YES;
    self.gMapView.settings.myLocationButton = YES;
    if (!self.mapService) {
        self.mapService = [[GBMapService alloc] initWithDelegate:self];
    }
   
}
-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    GMSCameraUpdate *vancouverCam = [GMSCameraUpdate setTarget:locationManager.location.coordinate zoom:12.0f];
    
    [self.gMapView moveCamera:vancouverCam];
    [self.gMapView animateWithCameraUpdate:vancouverCam];
    [locationManager stopUpdatingLocation];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)fromButtonTouchupInside:(id)sender {
    indexSelectLocation = 1;
    GMSAutocompleteViewController* acController = [[GMSAutocompleteViewController alloc]init];
    acController.delegate = self;
    [self presentViewController:acController animated:YES completion:nil];

}

- (IBAction)toButtonTouchupInside:(id)sender {
    indexSelectLocation = 2;
    GMSAutocompleteViewController* acController = [[GMSAutocompleteViewController alloc]init];
    acController.delegate = self;
    [self presentViewController:acController animated:YES completion:nil];

}
#pragma GMSAutocompleteDelegate
-(void) viewController:(GMSAutocompleteViewController *)viewController didAutocompleteWithPlace:(GMSPlace *)place{
    if (indexSelectLocation == 1) {
        [self.fromButton setTitle:place.formattedAddress forState:UIControlStateNormal];
        self.origin = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude);
        //    self.destination = CLLocationCoordinate2DMake(-33.86, 150.0);
        [self addMarkerToGoogleMapWithCLLocationCoordinate2D:self.origin address:place.formattedAddress];
    }
    else{
        [self.toButton setTitle:place.formattedAddress forState:UIControlStateNormal];
        self.destination = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude);
        [self addMarkerToGoogleMapWithCLLocationCoordinate2D:self.destination address:place.formattedAddress];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void) wasCancelled:(GMSAutocompleteViewController *)viewController{
    [self dismissViewControllerAnimated:YES completion:nil];

}
-(void) addMarkerToGoogleMapWithCLLocationCoordinate2D:(CLLocationCoordinate2D) location address:(NSString*)address{
    if (indexSelectLocation == 1) {
        if (_marker1 != nil) {
            self.marker1.map = nil;
        }
        self.marker1 = [GMSMarker markerWithPosition:location];
        self.marker1.title = address;
        self.marker1.icon = [UIImage imageNamed:@"greenMarker_ic"];
        self.marker1.map = self.gMapView;
        self.origin = location;
        self.gMapView.selectedMarker = _marker1;
        [self.gMapView animateWithCameraUpdate:
         [GMSCameraUpdate setTarget:self.marker1.position]];
    }
    else{
        if (self.market2 != nil) {
            self.market2.map = nil;
        }
        self.market2 = [GMSMarker markerWithPosition:location];
        self.market2.title = address;
        self.market2.icon = [UIImage imageNamed:@"redMarker_ic"];
        self.market2.map = self.gMapView;
        self.destination = location;
        self.gMapView.selectedMarker = _market2;
        [self.gMapView animateWithCameraUpdate:
         [GMSCameraUpdate setTarget:self.market2.position]];
        
    }
    
    if (self.marker1 != nil && self.market2 != nil) {
      //  [self.mapService routeFromOrigin:self.origin toDestination:self.destination];
       // [self.mapService getDistance:self.origin toDestination:self.destination];
        
    }
    
}

-(void) viewController:(GMSAutocompleteViewController *)viewController didFailAutocompleteWithError:(NSError *)error{
    NSLog(@"%@",error.description);
    [self dismissViewControllerAnimated:YES completion:nil];

}
@end
