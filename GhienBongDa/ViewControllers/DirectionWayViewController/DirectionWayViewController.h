//
//  DirectionWayViewController.h
//  GhienBongDa
//
//  Created by Tran Minh Tu on 5/26/17.
//  Copyright © 2017 TuTMT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "GBMapService.h"


@import GoogleMaps;
@interface DirectionWayViewController :BaseViewController<GMSMapViewDelegate,GBMapServiceDelegate,CLLocationManagerDelegate,GMSAutocompleteViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UIButton *fromButton;
@property (weak, nonatomic) IBOutlet UIButton *toButton;
@property (weak, nonatomic) IBOutlet GMSMapView *gMapView;
@property (nonatomic,strong) GBMapService* mapService;
@property (nonatomic, assign) CLLocationCoordinate2D origin;
@property (nonatomic, assign) CLLocationCoordinate2D destination;
@property (nonatomic,strong) GMSMarker *marker1;
@property (nonatomic,strong) GMSMarker *market2;
- (IBAction)fromButtonTouchupInside:(id)sender;
- (IBAction)toButtonTouchupInside:(id)sender;

@end
