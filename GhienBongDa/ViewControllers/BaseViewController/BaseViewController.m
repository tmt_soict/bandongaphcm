//
//  BaseViewController.m
//  Shoppie
//
//  Created by Le Quang Vinh on 11/13/13.
//
//

#import "BaseViewController.h"
#import <Google/Analytics.h>

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)initProperties {
    self.apiClient = [APIClient sharedClient];
	self.appDelegate = [Util appDelegate];
    self.appData = [AppData sharedInstance];
    self.progress = [[DRPLoadingSpinner alloc] init];
    
}

- (id)init {
    self = [super init];
    if (self) {
        // Custom initialization
        [self initProperties];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self initProperties];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.barTintColor = [UIColor navigationBarColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
     NSSetUncaughtExceptionHandler(&exceptionHandler);
    [self initProperties];
    if ([AppData sharedInstance].error != nil) {
        [Util showMessage:@"Tin bóng đá vừa bất ngờ thoát. Báo cáo tới đội ngũ phát triển" withTitle:@"Thông báo" style:UIAlertViewStyleDefault cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok" delegate:self andTag:100];
    }
}



BOOL exceptionAlertDismissed = FALSE;
void exceptionHandler(NSException *exception)
{
    
       [[AppData sharedInstance] setError:[NSString stringWithFormat:@"name:%@, reason:%@, userInfo : %@",exception.name,exception.reason,exception.userInfo]];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 100 && buttonIndex == 1)
    {
        
        [self sendEmailWithBody:[AppData sharedInstance].error];
    }
    else if(alertView.tag == 100 && buttonIndex == 0){
        [[AppData sharedInstance] setError:@""];
    }
}



- (void)sendEmailWithBody:(NSString *)body {
    // Email Subject
    NSString *emailTitle = @"Lỗi Crash 123 xe";
    // Email Content
    NSString *messageBody = body;
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@"dathv@vng.com.vn"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:^{
        [[AppData sharedInstance] setError:@""];
    }];
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}




- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setToolbarHidden:YES animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL) findErrorWithCode:(NSNumber *)code LocalizedDescription :(NSString *) localizedDescription
{
    NSString *codeString = [NSString stringWithFormat:@"%@",code];
    return [localizedDescription containsString:codeString];
}

- (void)setupUiBarForSlideMenuWithImageName:(NSString *)imageNamed
{
    UIButton * showMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [showMenuButton setImage:[UIImage filledImageFrom:[UIImage imageNamed:imageNamed]
                                            withColor:[UIColor whiteColor]]
                    forState:UIControlStateNormal];
    [showMenuButton setTitle:@"" forState:UIControlStateNormal];
    showMenuButton.frame = CGRectMake(0, 0, 20, 20);
    [showMenuButton addTarget:self action:@selector(showMenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * showMenuBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:showMenuButton];
    
    self.navigationItem.leftBarButtonItem = showMenuBarButtonItem;
}

- (void)showMenu
{
    // Dismiss keyboard (optional)
    //
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
}


-(void)pushToLoginViewControler{
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"LoginFlow" bundle:nil];
    UIViewController * initialViewController = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerStoryboardIdentifier"];
    [self.navigationController pushViewController:initialViewController animated:NO];

}
-(void)trackingGoogleAnalyticsWithName: (NSString*) nameViewController{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:nameViewController];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
-(void)trackingGAWithEvent:(NSString*)nameEvent value:(NSString*)value {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                          action:nameEvent
                                                           label:value
                                                           value:nil] build]];
}
//-(void)showMessageWithErrorCode:(SWGResponseError*) error{
//    switch ([error.code intValue]) {
//        case PHONE_EXISTED:
//            [Util showMessage:mPHONE_EXISTED withTitle:@"Thông Báo"];
//            break;
//        case PHONE_NOT_FOUND:
//            [Util showMessage:mPHONE_NOT_FOUND withTitle:@"Thông Báo"];
//            break;
//        case WRONG_PASSWORD:
//            [Util showMessage:mWRONG_PASSWORD withTitle:@"Thông Báo"];
//            break;
//        case NOT_PERMISSION:
//            [Util showMessage:mNOT_PERMISSION withTitle:@"Thông Báo"];
//            break;
//        case OTP_NOT_FOUND:
//            [Util showMessage:mOTP_NOT_FOUND withTitle:@"Thông Báo"];
//            break;
//            
//        default:
//            [Util showMessage:error.message withTitle:@"Thông Báo"];
//
//            break;
//    }
//}

@end
