//
//  RootDetailArticleViewController.m
//  SportsNews
//
//  Created by TuTMT on 10/31/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import "RootDetailArticleViewController.h"
#import "MenuViewController.h"
@interface RootDetailArticleViewController ()

@end

@implementation RootDetailArticleViewController{
}

-(void)awakeFromNib{

    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentController"];
    MenuViewController* tinLienQuanViewController = (MenuViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"menuController"];
    self.menuViewController = tinLienQuanViewController;
    [self setDirection:REFrostedViewControllerDirectionLeft];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    //[self.navigationController setTitle:self.title];
    [self.navigationController setNavigationBarHidden:YES];
       // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];

}
-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];


}
-(void)backToViewController:(NSNotification*) notification{
    [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController setNavigationBarHidden:NO];

}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) renewsData{
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backButtonTouchUpInside:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)menuButtonTouchUpInside:(id)sender {
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    [self.frostedViewController presentMenuViewController];
}
@end
