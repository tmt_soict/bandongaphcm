//
//  MenuViewController.h
//  GhienBongDa
//
//  Created by Tran Minh Tu on 5/25/17.
//  Copyright © 2017 TuTMT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "MenuItem.h"

@interface MenuViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;

@end
