//
//  MenuViewController.m
//  GhienBongDa
//
//  Created by Tran Minh Tu on 5/25/17.
//  Copyright © 2017 TuTMT. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuTableViewCell.h"
#import "HeaderMenuTableViewCell.h"
#import "HomeViewController.h"
#import "DirectionWayViewController.h"
#import "SupportViewController.h"
#import "PredictedFloodingViewController.h"
#import "NewFloodingPointViewController.h"
#import "SettingViewController.h"
@interface MenuViewController ()

@end

@implementation MenuViewController{
    NSMutableArray* menuArray;
    UINavigationController *navigationController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initMenuViewController];
    navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentController"];

    // Do any additional setup after loading the view.
}
-(void) initMenuViewController{
    menuArray = [[NSMutableArray alloc] init];
    menuArray = [self createMenuItems];
    self.menuTableView.delegate = self;
    self.menuTableView.dataSource = self;
    [self.menuTableView registerNib:[UINib nibWithNibName:@"MenuTableViewCell" bundle:nil] forCellReuseIdentifier:@"MenuTableViewCellIdentifiers"];

    [self.menuTableView registerNib:[UINib nibWithNibName:@"HeaderMenuTableViewCell" bundle:nil] forCellReuseIdentifier:@"HeaderMenuTableViewCellIdentifiers"];
}

- (NSMutableArray *)createMenuItems
{
    NSMutableArray * menuItems = [NSMutableArray new];
    
    MenuItem * menuItem = [MenuItem new];
    menuItem.title = @"Hiện trạng ngập";
    menuItem.normalImage = [UIImage imageNamed:@"home_forecast"];
    menuItem.menuIndex = menuIndexHome;
    [menuItems addObject:menuItem];
    
    menuItem = [MenuItem new];
    menuItem.title = @"Dự báo ngập";
    menuItem.normalImage = [UIImage imageNamed:@"ic_flag"];
    menuItem.menuIndex = menuIndexPreditect;
    [menuItems addObject:menuItem];
    
    menuItem = [MenuItem new];
    menuItem.title = @"Tìm đường tránh ngập";
    menuItem.normalImage = [UIImage imageNamed:@"ic_directions"];
    menuItem.menuIndex = menuIndexDirection;
    [menuItems addObject:menuItem];
    
    
    menuItem = [MenuItem new];
    menuItem.title = @"Báo điểm úng ngập";
    menuItem.normalImage = [UIImage imageNamed:@"ic_location_on"];
    menuItem.menuIndex = menuIndexNewFlood;
    [menuItems addObject:menuItem];
    
    menuItem = [MenuItem new];
    menuItem.title = @"Cài đặt";
    menuItem.normalImage = [UIImage imageNamed:@"ic_setting"];
    menuItem.menuIndex = menuIndexSetting;
    [menuItems addObject:menuItem];
    
    
    menuItem = [MenuItem new];
    menuItem.title = @"Hỗ trợ góp ý";
    menuItem.normalImage = [UIImage imageNamed:@"ic_supervisor"];
    menuItem.menuIndex = menuIndexSuport;
    [menuItems addObject:menuItem];
    
    
    
    return menuItems;
}
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 180;
    }
    else{
        return 50;
    }
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return menuArray.count + 1;
}
-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        HeaderMenuTableViewCell* headerCell = [tableView dequeueReusableCellWithIdentifier:@"HeaderMenuTableViewCellIdentifiers"];
        return headerCell;
    }
    else{
        MenuItem * menuItem = [menuArray objectAtIndex:(indexPath.row -1)];
        MenuTableViewCell* menuCell = [tableView dequeueReusableCellWithIdentifier:@"MenuTableViewCellIdentifiers"];
        menuCell.titleLabel.text = menuItem.title;
        menuCell.titleImage.image = menuItem.normalImage;
        return menuCell;
    }
    

}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row != 0) {
        MenuItem * menuItem = [menuArray objectAtIndex:(indexPath.row -1)];
        switch (menuItem.menuIndex) {
            case menuIndexHome:
                [self pushToHomeViewController];
                break;
            case menuIndexNewFlood:
                [self pushToNewFloodingPointViewController];
                break;
            case menuIndexSetting:
                [self pushToSettingViewController];
                break;
            case menuIndexDirection:
                [self pushToDirectionWayViewController];
                break;
            case menuIndexPreditect:
                [self pushToNewFloodingPointViewController];
                break;
            case menuIndexSuport:
                [self pushToSupportViewController];
                break;
            default:
                break;
        }
    }
    self.frostedViewController.contentViewController = navigationController;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) pushToHomeViewController{
    HomeViewController * homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewControllerStoryboardID"];
    navigationController.viewControllers = @[homeViewController];
    [self.frostedViewController hideMenuViewController];
}
-(void) pushToSupportViewController{
    SupportViewController * supportViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SupportViewControllerStoryboardID"];
    navigationController.viewControllers = @[supportViewController];
    [self.frostedViewController hideMenuViewController];
}
-(void) pushToSettingViewController{
    SettingViewController * settingViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewControllerStoryboardID"];
    navigationController.viewControllers = @[settingViewController];
    [self.frostedViewController hideMenuViewController];
}
-(void) pushToNewFloodingPointViewController{
    NewFloodingPointViewController * newFloodingPointViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NewFloodingPointViewControllerStoryboardID"];
    navigationController.viewControllers = @[newFloodingPointViewController];
    [self.frostedViewController hideMenuViewController];
}
-(void) pushToDirectionWayViewController{
    DirectionWayViewController * directionWayViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DirectionWayViewControllerStoryboardID"];
    navigationController.viewControllers = @[directionWayViewController];
    [self.frostedViewController hideMenuViewController];
}
@end
