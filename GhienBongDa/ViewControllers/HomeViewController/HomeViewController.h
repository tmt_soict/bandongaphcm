//
//  HomeViewController.h
//  GhienBongDa
//
//  Created by Tran Minh Tu on 5/25/17.
//  Copyright © 2017 TuTMT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@import GoogleMaps;
@interface HomeViewController : BaseViewController<CLLocationManagerDelegate,GMSMapViewDelegate,CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet GMSMapView *gMapView;



- (IBAction)menuButtonTouchupInside:(id)sender;
- (IBAction)homeviewPanGestureRecognize:(id)sender;

@end
