
//
//  HomeViewController.m
//  GhienBongDa
//
//  Created by Tran Minh Tu on 5/25/17.
//  Copyright © 2017 TuTMT. All rights reserved.
//

#import "HomeViewController.h"
#import "CameraMarkerView.h"

@interface HomeViewController ()

@end

@implementation HomeViewController{
    CLLocationManager* locationManager;
    NSMutableArray* markerArray;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    // Do any additional setup after loading the view.
}
-(void) initView{
    [self setupUiBarForSlideMenuWithImageName:@"navigation_ic"];
    [self prepareGoogleMaps];
    markerArray = [[NSMutableArray alloc] init];
    [self loadData];
}

- (void)prepareGoogleMaps
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    GMSCameraPosition *camera;
    if (locationManager != nil) {
        GMSCameraUpdate *vancouverCam = [GMSCameraUpdate setTarget:locationManager.location.coordinate zoom:12.0f];
        
        [self.gMapView moveCamera:vancouverCam];
        [self.gMapView animateWithCameraUpdate:vancouverCam];
    }
    else{
        camera = [GMSCameraPosition cameraWithLatitude:-33.86 longitude:151.20 zoom:12];
        
    }
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, 120, 0);
    [self.gMapView setPadding:edgeInsets];
    self.gMapView.myLocationEnabled = YES;
    self.gMapView.delegate = self;
    self.gMapView.indoorEnabled = YES;
    self.gMapView.accessibilityElementsHidden = NO;
    self.gMapView.settings.scrollGestures = YES;
    self.gMapView.settings.zoomGestures = YES;
    self.gMapView.settings.compassButton = YES;
    self.gMapView.settings.myLocationButton = YES;
    
    // self.gMapView.settings.myLocationButton = YES;
    // [self.googlemapView addSubview:self.gMapView];
    //self.view = self.gMapView;
    
    
    
    
    // [self.mapService routeFromOrigin:self.origin toDestination:self.destination];
}
-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    GMSCameraUpdate *vancouverCam = [GMSCameraUpdate setTarget:locationManager.location.coordinate zoom:12.0f];
    
    [self.gMapView moveCamera:vancouverCam];
    [self.gMapView animateWithCameraUpdate:vancouverCam];
    [locationManager stopUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) loadData{
    [self.apiClient getCurrentFloodInCityWithDate:[NSDate date] completionHandler:^(BaseClass *output, NSError *error, double errorCode) {
        if (error == nil) {
            if (errorCode == SUCCSESS) {
                int i = 0;
                for (ListCameras* cameraItem in output.listCameras) {
                    CLLocationCoordinate2D codinate = CLLocationCoordinate2DMake((CLLocationDegrees)cameraItem.lat, (CLLocationDegrees)cameraItem.lng);
                    [self addMarkerToGooleMapWithCordinate:codinate address:cameraItem.address index:i stopType:1];
                    i ++;
                    
                }
                for (int j = 0; j< output.listRainTracker.count; j++) {
                    ListRainTracker* raint = (ListRainTracker*) output.listRainTracker[j];
                    CLLocationCoordinate2D codinateRaint = CLLocationCoordinate2DMake((CLLocationDegrees)raint.lat, (CLLocationDegrees)raint.lng);
                    [self addMarkerToGooleMapWithCordinate:codinateRaint address:raint.tentram index:j stopType:2];


                }
                for (int n = 0; n < output.listTideTracker.count; n++) {
                    ListTideTracker* tideTracker = (ListTideTracker*) output.listTideTracker[n];
                    CLLocationCoordinate2D codinateTide = CLLocationCoordinate2DMake((CLLocationDegrees)tideTracker.lat, (CLLocationDegrees)tideTracker.lng);
                    [self addMarkerToGooleMapWithCordinate:codinateTide address:tideTracker.tentramtrieu index:n stopType:3];
                }
                
            }
        }
    }];
}



-(void) addMarkerToGooleMapWithCordinate:(CLLocationCoordinate2D) location address:(NSString*) address index:(int) index stopType:(int)type{
    GMSMarker* marker = [GMSMarker markerWithPosition:location];
    marker.title = address;
    
    if (type) {
        if (type == 1) {
            
            marker.icon = [UIImage imageNamed:@"green_marker"];
            
        }
        else if(type == 2){
            marker.icon = [UIImage imageNamed:@"red_marker"];
        }
        else{
            marker.icon = [UIImage imageNamed:@"default_marker"];
        }
    }
    
    marker.map = self.gMapView;
    [marker setZIndex:index];
    [markerArray addObject:marker];
    [self.gMapView animateWithCameraUpdate:
     [GMSCameraUpdate setTarget:marker.position]];
}


-(UIView*) mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker{
    CameraMarkerView* cameraView = (CameraMarkerView*) [[[NSBundle mainBundle] loadNibNamed:@"CameraMarkerView" owner:self options:nil] objectAtIndex:0];
    return cameraView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)menuButtonTouchupInside:(id)sender {
}

- (IBAction)homeviewPanGestureRecognize:(id)sender {
}
@end
