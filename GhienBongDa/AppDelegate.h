//
//  AppDelegate.h
//  GhienBongDa
//
//  Created by TuTMT on 11/7/16.
//  Copyright © 2016 TuTMT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;
@property (nonatomic,assign) BOOL fullScreenVideoIsPlaying;

@end

