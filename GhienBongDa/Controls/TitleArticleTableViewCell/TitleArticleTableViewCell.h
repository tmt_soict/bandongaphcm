//
//  TitleArticleTableViewCell.h
//  GhienBongDa
//
//  Created by TuTMT on 11/24/16.
//  Copyright © 2016 TuTMT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TitleArticleTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleArticle;
@property (strong, nonatomic) IBOutlet UILabel *timeLable;


-(void)setupTitleArticleTableViewCellWithTitle:(NSString*)title timeRemain:(NSString*)timeRemain;
@end
