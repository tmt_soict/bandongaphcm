//
//  TitleArticleTableViewCell.m
//  GhienBongDa
//
//  Created by TuTMT on 11/24/16.
//  Copyright © 2016 TuTMT. All rights reserved.
//

#import "TitleArticleTableViewCell.h"

@implementation TitleArticleTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupTitleArticleTableViewCellWithTitle:(NSString*)title timeRemain:(NSString*)timeRemain{
    self.titleArticle.text = title;
    self.timeLable.text = timeRemain;
}

@end
