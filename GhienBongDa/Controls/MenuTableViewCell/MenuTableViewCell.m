//
//  MenuTableViewCell.m
//  GhienBongDa
//
//  Created by Tran Minh Tu on 5/25/17.
//  Copyright © 2017 TuTMT. All rights reserved.
//

#import "MenuTableViewCell.h"

@implementation MenuTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
