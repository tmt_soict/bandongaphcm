//
//  MenuTableViewCell.h
//  GhienBongDa
//
//  Created by Tran Minh Tu on 5/25/17.
//  Copyright © 2017 TuTMT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *titleImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
