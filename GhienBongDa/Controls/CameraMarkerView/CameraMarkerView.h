//
//  CameraMarkerView.h
//  GhienBongDa
//
//  Created by Tran Minh Tu on 5/28/17.
//  Copyright © 2017 TuTMT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CameraMarkerView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *cameraImage;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;

@end
